# HTTP API description

## Headers

`Authorization` header need to be provided on each request.

`Authorization`, is basic authentication username/password combination to the backend.

---

## Common result

`400`, on ???, no content

`401`, on invalid credentials, no content => adjust credentials

`404`, on not found, no content => adjust request

`504`, on backend timeout, no content => resend request after pause

---

## Objects

### Download object

#### Query

`GET` /*objects*/path/to/my/object

#### Result

`200`, on success, object as content

### Upload object

#### Query

`PUT` /*objects*/path/to/my/object?force&checksum&pid

#### Parameters (optional)

`force`, is needed to allow overwrite.

`checksum`, will calculate and return the SHA256 checksum in `X-Checksum` header.

`pid`, will query and return the PID (if any) in `X-PID` header.

#### Result

`204`, on success, no content

`409`, on conflict, error message => use `force` flag to allow overwrite

### Delete object

#### Query

`DELETE` /*objects*/path/to/my/object

#### Result

`200`, on success, no content

---

## Collections

### List collection

#### Query

`GET` /*collections*/path/to/my/collection

#### Parameters (optional)

`recursive`, will list collection content recursively.

#### Result

`200`, on success, JSON array as content

`422`, on data object, no content => adjust request

### Create collection

#### Query

`PUT` /*collections*/path/to/my/collection

#### Parameters (optional)

`recursive`, will create missing collection paths.

#### Result

`200`, on success, no content

`409`, on conflict, error message => use `recursive` flag to allow collection path creation

`422`, on data object, no content => adjust request

### Delete collection

#### Query

`DELETE` /*collections*/path/to/my/collection

#### Parameters (optional)

`recursive`, will delete collection content recursively.

`nothrash`, will delete permanently, bypassing possible thrash.

#### Result

`200`, on success, no content

`409`, on conflict, error message => use `recursive` flag to allow recursive deletion

`422`, on data object, no content => adjust request

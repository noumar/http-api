# HTTP-API

## Usage

* Clone with: `git clone --recursive https://gitlab.com/noumar/http-api.git`
* Update package index with: `cabal new-update`
* Build with: `cabal new-build s3`
* Execute with: `cabal new-exec s3`
* OR
* Run (build and exec) with: `cabal new-run s3`
* Start a REPL with: `cabal new-repl s3`

## Optimization

* Default optimization is normal (-O1)
* While developing disable optimization (-O0) with: `cabal new-configure --disable-optimization`
  * Will write a `cabal.project.local` file, storing the setting
* For production use full optimization (-O2) with: `cabal new-build s3 -O2`
